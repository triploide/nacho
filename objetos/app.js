class Personaje {
    constructor (n) {
        this.x = 0;
        this.y = 0;
        this.nombre = n 
    }

    saludar () {
        console.log ("mi nombre es " + this.nombre);
    } 

    mover (x,y) {
        this.x = this.x + x
        this.y = this.y + y 
     }
}

let mario = new Personaje("mario");
let moria = new Personaje("moria");

mario.mover(20,30)
console.log(mario)

mario.mover(40,-10)
console.log(mario)

moria.mover(30,60)
console.log(moria)





